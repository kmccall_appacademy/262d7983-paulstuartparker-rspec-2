
def reverser(&prc)
  string = prc.call
  new_s = string.split(" ")
  new_s.map { |x| x.split("").reverse.join("") }.join(" ")
end

def adder(to_add = 1, &prc)
  number = prc.call
  number += to_add
end

def repeater(n = 1, &prc)
  n.times do
    prc.call
  end
end
